/*
 * File:        main_ML8511.c
 * Author:      Brandon Ruiz Vasquez
 * Compiler:    XC8 v2.1    
 * Link C lib:  C90
 *              
 * Created on June 13, 2020, 1:58 AM
 */

#define USE_OR_MASKS    //Masks for ADC library configuration
#define CLOCK_20MHZ
#include "Alteri.h"     //Fuses configuration
#include <stdlib.h>     //Contains itoa
#include <stdio.h>      //Contains sprintf
#include "PCD8544.h"    //LCD library  
#include "adc.h"        //ADC library

char Result[6];
unsigned int ADCResult=300;
float UV_INTENSITY;
char UV_INTENSITY_S[10];
int status[2];

void main(void) {
    while (1) {
        OpenADC(ADC_FOSC_4 | ADC_RIGHT_JUST | ADC_20_TAD, ADC_CH12 | ADC_INT_OFF | ADC_REF_VDD_VSS, ADC_13ANA);
        //Activate ADC
        ConvertADC();       //Start ADC conversion
        while(BusyADC());   //Wait until ADC is done
        ADCResult = (unsigned int) ReadADC();   //Reads ADC range 0-1023
        
        UV_INTENSITY = (75.0/2048.0)*(ADCResult - 1024.0/5.0);  
        //function to convert ADC to mW/cm2 Simplified
        //((15-0)/(1024*(3/5-1/5)))*(x - 1024/5)    Original
        
        CloseADC();     //Disables ADC BUT doesn't make Channels digital
        ADCON1bits.PCFG = 0b1111;   //Makes Channels digital
        LCD_NOKIA_Init();           
        LCD_NOKIA_Clear();
        LCD_NOKIA_WriteString("Hello!1",0,0);   //Test string
        itoa(Result,ADCResult,10);              //Convert int to ascii
        sprintf(UV_INTENSITY_S,"%2.2f mW/cm2",UV_INTENSITY);    
        //Makes string to display results
        LCD_NOKIA_WriteString(UV_INTENSITY_S,0,1);  //Display Sensor result
        LCD_NOKIA_WriteString(Result,0,2);          //Displays ADC result
        delay_ms(1000);
        //LCD_NOKIA_Clear();
    }
    return;
}
